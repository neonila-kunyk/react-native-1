import AsyncStorage from '@react-native-community/async-storage'
import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist'
import usersReducer from './reducers/users-reducer'
import contactsReducer from './reducers/contacts-reducer'

const rootReducer = combineReducers({ users: usersReducer, contacts: contactsReducer })

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }
  })
})
export const persistor = persistStore(store)
