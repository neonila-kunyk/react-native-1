import { createSlice } from '@reduxjs/toolkit'
import { v4 as uuidv4 } from 'uuid'
import 'react-native-get-random-values'
import { NONE } from '../../constants'
import data from '../../data.json'

const contactsSlice = createSlice({
  name: 'contacts',
  initialState: {
    contacts: [],
    filter: NONE,
    currentContactId: NONE
  },
  reducers: {
    loadContacts(state) {
      if (!state.contacts.length) {
        const contacts = []
        data.forEach((user) => {
          data.forEach((contact) => {
            if (contact.id !== user.id) {
              contacts.push({
                id: uuidv4(),
                userId: user.id,
                name: contact.name,
                email: contact.email,
                phone: contact.phone
              })
            }
          })
        })
        state.contacts = contacts
      }
    },
    addContact(state, action) {
      const { userId, contact } = action.payload
      contact.id = uuidv4()
      contact.userId = userId
      state.contacts.push(contact)
    },
    editContact(state, action) {
      const { id, newInfo } = action.payload
      const oldContact = state.contacts.find((contact) => contact.id === id)
      const index = state.contacts.indexOf(oldContact)
      state.contacts.splice(index, 1, { ...oldContact, ...newInfo })
    },
    deleteContact(state, action) {
      const { id } = action.payload
      state.contacts = state.contacts.filter((contact) => contact.id !== id)
    },
    setFilter(state, action) {
      const { filter } = action.payload
      state.filter = filter
    },
    clearFilter(state) {
      state.filter = NONE
    },
    setCurrentContactId(state, action) {
      const { id } = action.payload
      state.currentContactId = id
    },
    removeCurrentContactId(state) {
      state.currentContactId = NONE
    }
  }
})

export const {
  loadContacts, addContact, editContact, deleteContact, setFilter, clearFilter, setCurrentContactId, removeCurrentContactId
} = contactsSlice.actions
export default contactsSlice.reducer
