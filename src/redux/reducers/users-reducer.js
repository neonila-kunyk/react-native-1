/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'
import { UNREGISTRATED, UNAUTHORIZED } from '../../constants'
import data from '../../data.json'

const usersSlice = createSlice({
  name: 'users',
  initialState: {
    users: [],
    currentUserId: UNAUTHORIZED
  },
  reducers: {
    loadUsers(state) {
      if (!state.users.length) {
        data.map((user) => user.password = `password-${user.id}`)
        state.users = data
      }
    },
    checkIsUserExist(state, action) {
      let { email, password } = action.payload
      email = email.trim().toLowerCase()
      password = password.trim()
      const currentUser = state.users.find((user) => user.email === email && user.password === password)
      if (currentUser) state.currentUserId = currentUser.id
      else state.currentUserId = UNREGISTRATED
    },
    removeCurrentUserId(state) {
      state.currentUserId = UNAUTHORIZED
    }
  }
})

export const { loadUsers, checkIsUserExist, removeCurrentUserId } = usersSlice.actions
export default usersSlice.reducer
