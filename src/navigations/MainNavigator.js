import { createStackNavigator } from 'react-navigation-stack'
import Contacts from '../views/Contacts/Contacts'
import Details from '../views/Details/Details'

const MainNavigator = createStackNavigator(
  {
    Contacts: {
      screen: Contacts,
      navigationOptions: {
        headerShown: false
      }
    },
    Details: {
      screen: Details,
      navigationOptions: {
        headerShown: false
      }
    }
  },
  {
    initialRouteName: 'Contacts'
  }
)

export default MainNavigator
