import { createSwitchNavigator } from 'react-navigation'
import SignIn from '../views/SignIn/SignIn'
import mainNavigator from './MainNavigator'

const AppNavigator = createSwitchNavigator(
  {
    Main: mainNavigator,
    Auth: SignIn
  },
  {
    initialRouteName: 'Auth'
  }
)

export default AppNavigator
