import React from 'react'
import { TextInput, View, Text } from 'react-native'
import PropTypes from 'prop-types'
import styles from './styles'

const DetailsLine = ({
  label, value, onChangeText, isEditing
}) => (
  <View style={styles.container}>
    <Text style={styles.label}>{label}</Text>
    <TextInput
      style={styles.input}
      underlineColorAndroid="transparent"
      onChangeText={onChangeText}
      value={value}
      editable={isEditing}
    />
  </View>
)

DetailsLine.defaultProps = {
  label: '',
  value: '',
  isEditing: false,
  onChangeText: () => {}
}

DetailsLine.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  isEditing: PropTypes.bool,
  onChangeText: PropTypes.func
}

export default DetailsLine
