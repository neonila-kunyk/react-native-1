import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 2
  },
  label: {
    marginRight: 'auto',
    color: '#111',
    marginTop: '1.5rem'
  },
  input: {
    width: '100%',
    height: '2rem',
    fontSize: '1rem',
    color: '#111',
    padding: 0
  }
})
