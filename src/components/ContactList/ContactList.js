import React from 'react'
import { FlatList } from 'react-native'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import ContactCard from '../ContactCard/ContactCard'
import styles from './styles'

const ContactList = ({ showDetails }) => {
  const { currentUserId } = useSelector((state) => state.users)
  const { filter } = useSelector((state) => state.contacts)
  let { contacts } = useSelector((state) => state.contacts)
  contacts = contacts.filter((contact) => contact.userId === currentUserId)
  if (filter) {
    contacts = contacts.filter((contact) => contact.name.toLowerCase().includes(filter.toLowerCase()))
  }
  const renderCard = ({ item }) => <ContactCard name={item.name} email={item.email} id={item.id} showDetails={showDetails} />
  return (
    <FlatList
      data={contacts}
      extraData={contacts}
      keyExtractor={(item) => item.id}
      renderItem={renderCard}
      contentContainerStyle={{ alignItems: 'center' }}
      style={styles.container}
    />
  )
}

ContactList.defaultProps = {
  showDetails: () => {}
}

ContactList.propTypes = {
  showDetails: PropTypes.func
}

export default ContactList
