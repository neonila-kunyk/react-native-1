import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    marginTop: '1rem',
    marginHorizontal: '1rem'
  }
})
