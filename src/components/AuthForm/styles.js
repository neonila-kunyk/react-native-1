import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    width: '100%',
    marginTop: '1.5rem'
  },
  button: {
    width: '100%',
    height: '2.5rem',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
    backgroundColor: '#0062b8',
    marginTop: '2rem'
  },
  buttonText: {
    fontSize: '1.25rem',
    fontWeight: 'bold',
    color: 'white'
  },
  inputLabel: {
    marginRight: 'auto',
    color: '#0062b8'
  }
})
