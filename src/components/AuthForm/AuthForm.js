import React, { useState } from 'react'
import {
  Text, Pressable, Alert, View
} from 'react-native'
import { useDispatch } from 'react-redux'
import EmailInput from '../EmailInput/EmailInput'
import PasswordInput from '../PasswordInput/PasswordInput'
import { checkIsUserExist } from '../../redux/reducers/users-reducer'
import styles from './styles'

const AuthForm = () => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const onChangeEmail = (text) => {
    setEmail(text)
  }
  const onChangePassword = (text) => {
    setPassword(text)
  }

  const onSignIn = () => {
    if (!email || !password) Alert.alert('Email and password are required')
    else dispatch(checkIsUserExist({ email, password }))
  }

  return (
    <View style={styles.container}>
      <Text style={styles.inputLabel}>Email</Text>
      <EmailInput onChangeText={onChangeEmail} value={email} />
      <Text style={styles.inputLabel}>Password</Text>
      <PasswordInput onChangeText={onChangePassword} value={password} />
      <Pressable style={styles.button} onPress={onSignIn}>
        <Text style={styles.buttonText}>Sign In</Text>
      </Pressable>
    </View>
  )
}

export default AuthForm
