import React from 'react'
import { TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import styles from './styles'

const PasswordInput = ({ onChangeText, value }) => {
  const [visible, setVisibility] = React.useState(false)

  const icon = !visible ? 'eye-slash' : 'eye'

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        onChangeText={onChangeText}
        value={value}
        secureTextEntry={!visible}
      />
      <Icon
        name={icon}
        onPress={() => setVisibility(!visible)}
        style={styles.icons}
      />
    </View>
  )
}

PasswordInput.defaultProps = {
  value: '',
  onChangeText: () => {}
}

PasswordInput.propTypes = {
  value: PropTypes.string,
  onChangeText: PropTypes.func
}

export default PasswordInput
