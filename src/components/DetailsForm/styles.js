import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    marginTop: '1rem',
    marginBottom: '5rem',
    marginHorizontal: '1rem'
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: '1rem'
  }
})
