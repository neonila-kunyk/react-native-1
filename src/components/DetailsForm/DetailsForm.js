import React, { useState } from 'react'
import { View, Alert } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import DetailsLine from '../DetailsLine/DetailsLine'
import Button from '../Button/Button'
import {
  addContact, editContact, deleteContact, removeCurrentContactId
} from '../../redux/reducers/contacts-reducer'
import styles from './styles'

const DetailsForm = ({ goBack, isCreatingContact }) => {
  const dispatch = useDispatch()
  const [isEditing, setIsEditing] = useState(false)
  const { currentUserId } = useSelector((state) => state.users)
  const { currentContactId, contacts } = useSelector((state) => state.contacts)
  const currentContact = isCreatingContact ? null : contacts.find((contact) => contact.id === currentContactId)
  const [name, setName] = useState(isCreatingContact ? '' : currentContact.name)
  const [phone, setPhone] = useState(isCreatingContact ? '' : currentContact.phone)
  const [email, setEmail] = useState('')

  const onChangeName = (text) => {
    setName(text)
  }
  const onChangePhone = (text) => {
    setPhone(text)
  }
  const onChangeEmail = (text) => {
    setEmail(text)
  }

  const onConfirm = () => {
    setIsEditing(false)
    if (!name || !phone || (isCreatingContact && !email)) Alert.alert('All fields are required')
    else if (isCreatingContact) {
      dispatch(addContact({ userId: currentUserId, contact: { name, phone, email } }))
      goBack()
    } else dispatch(editContact({ id: currentContactId, newInfo: { name, phone } }))
  }

  const onEdit = () => {
    setIsEditing(true)
  }

  const onDelete = () => {
    goBack()
    dispatch(removeCurrentContactId())
    dispatch(deleteContact({ id: currentContactId }))
  }

  const type = isCreatingContact || isEditing

  return (
    <View style={styles.container}>
      <DetailsLine label="Name" value={name} isEditing={type} onChangeText={onChangeName} />
      <DetailsLine label="Phone number" value={phone} isEditing={type} onChangeText={onChangePhone} />
      {isCreatingContact && <DetailsLine label="Email" value={email} isEditing={type} onChangeText={onChangeEmail} />}
      {type
        ? (
          <View style={styles.buttons}>
            <Button text="confirm" icon="check" onPress={onConfirm} />
          </View>
        )
        : (
          <View style={styles.buttons}>
            <Button text="edit" icon="pencil" onPress={onEdit} />
            <Button text="delete" icon="trash" onPress={onDelete} />
          </View>
        )}
    </View>
  )
}

DetailsForm.defaultProps = {
  goBack: () => { },
  isCreatingContact: false
}

DetailsForm.propTypes = {
  goBack: PropTypes.func,
  isCreatingContact: PropTypes.bool
}

export default DetailsForm
