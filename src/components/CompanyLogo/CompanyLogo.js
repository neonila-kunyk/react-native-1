import React from 'react'
import { View, Text, Image } from 'react-native'
import PropTypes from 'prop-types'
import styles from './styles'

const CompanyLogo = ({ logoUri, name }) => (
  <View style={styles.container}>
    <Image
      style={styles.logo}
      source={{ uri: logoUri }}
    />
    <Text style={styles.name}>{name}</Text>
  </View>
)

CompanyLogo.defaultProps = {
  logoUri: 'https://www.freepnglogos.com/uploads/logo-3d-png/3d-company-logos-design-logo-online-2.png'
}

CompanyLogo.propTypes = {
  logoUri: PropTypes.string,
  name: PropTypes.string.isRequired
}

export default CompanyLogo
