import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    width: '100%',
    height: '15%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: '20%',
    height: '60%',
    borderRadius: 1000,
    resizeMode: 'contain'
  },
  name: {
    fontSize: '1.5rem'
  }
})
