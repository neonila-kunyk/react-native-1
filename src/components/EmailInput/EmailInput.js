import React from 'react'
import { TextInput, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from './styles'

const EmailInput = ({ onChangeText, value }) => (
  <View style={styles.container}>
    <TextInput
      style={styles.input}
      underlineColorAndroid="transparent"
      onChangeText={onChangeText}
      value={value}
      keyboardType="email-address"
    />
  </View>
)

EmailInput.defaultProps = {
  value: '',
  onChangeText: () => {}
}

EmailInput.propTypes = {
  value: PropTypes.string,
  onChangeText: PropTypes.func
}

export default EmailInput
