import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 2
  },
  input: {
    flex: 1,
    height: '2.5rem',
    fontSize: '1rem',
    color: '#111',
    paddingHorizontal: 0
  }
})
