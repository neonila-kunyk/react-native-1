import React from 'react'
import { Text, Pressable } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import styles from './styles'

const Button = ({ text, icon, onPress }) => (
  <Pressable style={styles.button} onPress={onPress}>
    <Text style={styles.buttonText}>{text.toUpperCase()}</Text>
    <Icon
      name={icon}
      style={styles.icon}
    />
  </Pressable>
)

Button.defaultProps = {
  text: '',
  icon: '',
  onPress: () => {}
}

Button.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.string,
  onPress: PropTypes.func
}

export default Button
