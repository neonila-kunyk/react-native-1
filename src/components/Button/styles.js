import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  button: {
    flexDirection: 'row',
    width: '7rem',
    height: '2.25rem',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#666',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
    backgroundColor: '#dee7ef',
    marginTop: '1rem'
  },
  buttonText: {
    fontSize: '1rem',
    color: '#666'
  },
  icon: {
    marginLeft: '0.5rem',
    height: '2rem',
    fontSize: '1rem',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#666'
  }
})
