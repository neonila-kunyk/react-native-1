import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    borderColor: '#dee7ef',
    borderWidth: 2,
    borderRadius: 25,
    marginVertical: '2rem',
    marginHorizontal: '1rem'
  },
  icons: {
    width: 30,
    height: '2rem',
    fontSize: '1.2rem',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#666',
    marginRight: '0.3rem'
  },
  input: {
    flex: 1,
    height: '2.5rem',
    alignSelf: 'stretch',
    fontSize: '1rem',
    color: '#111',
    paddingHorizontal: 0
  }
})
