import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { TextInput, View, Keyboard } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { setFilter, clearFilter } from '../../redux/reducers/contacts-reducer'
import styles from './styles'

const Search = () => {
  const dispatch = useDispatch()
  const [search, setSearch] = useState('')

  const onChangeSearch = (text) => {
    setSearch(text)
  }

  const onSearch = () => {
    Keyboard.dismiss()
    dispatch(setFilter({ filter: search }))
  }

  const onClear = () => {
    setSearch('')
    dispatch(clearFilter())
  }

  return (
    <View style={styles.container}>
      <Icon
        name="search"
        onPress={onSearch}
        style={styles.icons}
      />
      <TextInput
        style={styles.input}
        placeholder="Search"
        placeholderTextColor="#777"
        underlineColorAndroid="transparent"
        onChangeText={onChangeSearch}
        onSubmitEditing={onSearch}
        value={search}
      />
      <Icon
        name="times"
        onPress={onClear}
        style={styles.icons}
      />
    </View>
  )
}

export default Search

// import React, { useEffect, useState } from 'react'
// import { SafeAreaView, Text, Alert } from 'react-native'
// import { useDispatch, useSelector } from 'react-redux'
// import CompanyLogo from '../../components/CompanyLogo/CompanyLogo'
// import AuthForm from '../../components/AuthForm/AuthForm'
// import { loadContacts, setFilter, clearFilter } from '../../redux/reducers/users-reducer'
// import { UNREGISTRATED, UNAUTHORIZED } from '../../constants'
// import styles from './styles'

// const Contacts = ({ navigation }) => {
//   const dispatch = useDispatch()
//   const [search, setSearch] = useState('')
//   const { contacts, filter } = useSelector((state) => state.contacts)

//   useEffect(() => {
//     dispatch(loadContacts())
//   }, [])

//   useEffect(() => {
//     if (currentUserId === UNREGISTRATED) Alert.alert('Invalid email or password')
//     else if (currentUserId !== UNAUTHORIZED) navigation.navigate('Main')
//   }, [filter])

//   const onChangeSearch = (text) => {
//     setSearch(text)
//   }

//   const onSignIn = () => {
//     if (!email || !password) Alert.alert('Email and password are required')
//     else dispatch(checkIsUserExist({ email, password }))
//   }

//   return (
//     <SafeAreaView style={styles.container}>
//       <Text style={styles.inputLabel}>Email</Text>
//       <EmailInput onChangeText={onChangeEmail} value={email} />
//       <Text style={styles.inputLabel}>Password</Text>
//       <PasswordInput onChangeText={onChangePassword} value={password} />
//       <Pressable style={styles.button} onPress={onSignIn}>
//         <Text style={styles.buttonText}>Sign In</Text>
//       </Pressable>
//     </SafeAreaView>
//   )
// }

// export default Contacts
