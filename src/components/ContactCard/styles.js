import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    height: '3.5rem',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: '0.5rem',
    marginHorizontal: '1rem',
    padding: '0.5rem',
    paddingLeft: '1.5rem',
    backgroundColor: '#dee7ef'
  },
  icons: {
    width: 35,
    fontSize: '2rem',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#666'
  },
  i: {
    fontSize: '1.25rem'
  },
  info: {
    flex: 1,
    alignItems: 'flex-start',
    marginLeft: '1rem'
  },
  name: {
    fontSize: '1.25rem',
    color: '#111'
  },
  email: {
    fontSize: '0.8rem',
    color: '#666'
  }
})
