import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { useDispatch } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import { setCurrentContactId } from '../../redux/reducers/contacts-reducer'
import styles from './styles'

const ContactCard = ({
  name, email, id, showDetails
}) => {
  const dispatch = useDispatch()

  const onContactPress = () => {
    dispatch(setCurrentContactId({ id }))
    showDetails()
  }

  return (
    <TouchableOpacity style={styles.container} onPress={onContactPress}>
      <Icon
        name="user-circle"
        style={styles.icons}
      />
      <View style={styles.info}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.email}>{email}</Text>
      </View>
      <Icon
        name="info-circle"
        style={[styles.icons, styles.i]}
      />
    </TouchableOpacity>
  )
}

ContactCard.defaultProps = {
  name: '',
  email: '',
  showDetails: () => {}
}

ContactCard.propTypes = {
  name: PropTypes.string,
  email: PropTypes.string,
  id: PropTypes.string.isRequired,
  showDetails: PropTypes.func
}

export default ContactCard
