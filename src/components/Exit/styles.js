import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  button: {
    width: '2.5rem',
    height: '2rem',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3
  },
  icon: {
    marginLeft: '0.5rem',
    height: '2rem',
    fontSize: '2rem',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#111',
    transform: [
      { scaleX: -1 }
    ]
  }
})
