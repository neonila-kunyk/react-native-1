import React from 'react'
import { Pressable } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import styles from './styles'

const Exit = ({ onPress }) => (
  <Pressable style={styles.button} onPress={onPress}>
    <Icon
      name="sign-out"
      style={styles.icon}
    />
  </Pressable>
)

Exit.defaultProps = {
  onPress: () => {}
}

Exit.propTypes = {
  onPress: PropTypes.func
}

export default Exit
