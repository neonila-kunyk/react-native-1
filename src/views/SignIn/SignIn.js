import React, { useEffect } from 'react'
import { SafeAreaView, Text, Alert } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import CompanyLogo from '../../components/CompanyLogo/CompanyLogo'
import AuthForm from '../../components/AuthForm/AuthForm'
import { loadUsers } from '../../redux/reducers/users-reducer'
import { UNREGISTRATED, UNAUTHORIZED } from '../../constants'
import styles from './styles'

const SignIn = ({ navigation }) => {
  const dispatch = useDispatch()
  const { currentUserId } = useSelector((state) => state.users)

  useEffect(() => {
    dispatch(loadUsers())
  }, [])

  useEffect(() => {
    if (currentUserId === UNREGISTRATED) Alert.alert('Invalid email or password')
    else if (currentUserId !== UNAUTHORIZED) navigation.navigate('Main')
  }, [currentUserId])

  return (
    <SafeAreaView style={styles.container}>
      <CompanyLogo name="Phonebook" logoUri="http://assets.stickpng.com/images/5a452570546ddca7e1fcbc7d.png" />
      <Text style={styles.signIn}>Sign In</Text>
      <Text style={styles.greeting}>Hi there! Nice to see you again.</Text>
      <AuthForm />
    </SafeAreaView>
  )
}

export default SignIn
