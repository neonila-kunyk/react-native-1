import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#eee',
    padding: '2rem'
  },
  signIn: {
    fontSize: '2rem',
    fontWeight: 'bold',
    marginTop: '1.5rem'
  },
  greeting: {
    fontSize: '1.25rem',
    color: '#777',
    marginTop: '0.5rem'
  }
})
