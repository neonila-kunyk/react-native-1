import React, { useEffect } from 'react'
import { SafeAreaView } from 'react-native'
import { useDispatch } from 'react-redux'
import Search from '../../components/Search/Search'
import ContactList from '../../components/ContactList/ContactList'
import Button from '../../components/Button/Button'
import { loadContacts, setCurrentContactId } from '../../redux/reducers/contacts-reducer'
import { removeCurrentUserId } from '../../redux/reducers/users-reducer'
import { CREATING } from '../../constants'
import styles from './styles'

const Contacts = ({ navigation }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadContacts())
  }, [])

  const showDetails = () => {
    navigation.navigate('Details')
  }

  const onCreate = () => {
    dispatch(setCurrentContactId({ id: CREATING }))
    navigation.navigate('Details')
  }

  const onSignOut = () => {
    dispatch(removeCurrentUserId())
    navigation.navigate('Auth')
  }

  return (
    <SafeAreaView style={styles.container}>
      <Search />
      <ContactList showDetails={showDetails} />
      <Button text="create" icon="plus" onPress={onCreate} />
      <Button text="sign out" icon="arrow-right" onPress={onSignOut} />
    </SafeAreaView>
  )
}

export default Contacts
