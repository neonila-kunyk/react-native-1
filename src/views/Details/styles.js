import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#eee',
    padding: '2rem'
  },
  avatar: {
    width: '40%',
    height: '20%',
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: '1rem'
  }
})
