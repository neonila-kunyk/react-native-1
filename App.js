import React from 'react'
import { Provider } from 'react-redux'
import EStyleSheet from 'react-native-extended-stylesheet'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './src/redux/store'
import AppNavigator from './src/navigations'

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <AppNavigator />
    </PersistGate>
  </Provider>
)

EStyleSheet.build()

export default App
